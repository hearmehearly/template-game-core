﻿using System;
using UnityEngine;


namespace Core
{
    [Serializable]
    public class ScriptableObjectBaseData<K>
    {
        public K key = default;
    };


    public abstract class ScriptableObjectData<D, K> : ScriptableObject where D : ScriptableObjectBaseData<K>
    {
        [SerializeField] protected D[] data = default;


        public D FindData(K type)
        {
            D foundData = Array.Find(data, e => type.Equals(e.key));

            if (foundData == null)
            {
                Debug.LogWarning($"No data found for {type} in {this}");
            }

            return foundData;
        }
    }
}