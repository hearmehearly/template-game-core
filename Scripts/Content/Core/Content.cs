﻿using UnityEngine;


namespace Core
{
    public static class Content
    {
        private static IContentStorage Storage { get; set; }

        public static IContentManagement Management { get; set; }


        public static bool IsDataEmpty =>
            Storage == null || Management == null;

        public static void SetupContentStorage(IContentStorage _storage)
        {
            Storage = _storage;
        }


        public static void SetupContentManagement(IContentManagement _management)
        {
            Management = _management;
        }


        public static T GetSettings<T>() where T : class, IContentStorage
        {
            if (Storage == null)
            {
                Debug.Log($"Storage is not injected");
            }

            return Storage as T;
        }


        public static T GetManagement<T>() where T : class, IContentManagement
        {
            if (Management == null)
            {
                Debug.Log($"Management is not injected");
            }

            return Management as T;
        }
    }
}
