﻿namespace Core
{
    public interface IContentImport
    {
        void ReimportContent();
    }
}