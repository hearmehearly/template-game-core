using UnityEngine;

namespace Core
{
    public interface IContentManagement
    {
        T CreateObject<T>(T prefab, Vector3 position, Quaternion rotation, Transform parent = null) where T : Component;

        void DestroyObject(GameObject objectToDestroy);
    }
}
