﻿namespace Core
{
    public enum CustomPrefabType
    {
        None                    = 0,
        ShotLineRenderer        = 2,
        ShooterScope            = 3,
        ArcPoint                = 4,
        JointMarker             = 5,
        OuterCorner             = 6,
        InnerCorner             = 7,
        CustomLoader            = 8,
        AcidDrop                = 9,
        GravyShotLineRenderer   = 10,
        RopeSegment             = 11,
        TransitionView          = 12,
        HelpBubble              = 13,
        PortalObject            = 14,
        BonusView               = 15,
        SkinBundleShopCell      = 16,
        CurrencyBundleShopCell  = 17,
        NoAdsShopCell           = 18,
        MansionLock             = 19
    }
}
