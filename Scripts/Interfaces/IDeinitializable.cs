﻿namespace Core
{
    public interface IDeinitializable
    {
        void Deinitialize();
    }
}
