﻿namespace Core
{
    public interface IButtonsHolder
    {
        void InitializeButtons();

        void DeinitializeButtons();
    }
}
