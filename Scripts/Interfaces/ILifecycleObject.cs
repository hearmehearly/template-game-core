﻿namespace Core
{
    public interface ILifecycleObject : IInitializable, IDeinitializable
    {

    }
}
