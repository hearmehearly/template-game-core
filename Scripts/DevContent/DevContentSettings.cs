﻿using UnityEngine;


[CreateAssetMenu(fileName = "DevContentSettings ", menuName = "Settings/DevContent/DevContentSettings")]
public class DevContentSettings : ScriptableObject
{
    public bool enabledInTrainingRoom = default;

    public DevHitVisual devHitVisual = default;
    public float hitDuration = default;

    [Range(0, 1)]
    public float colorAlpha = default;
}
