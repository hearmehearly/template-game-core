﻿using UnityEngine;


public class DevHitVisual : MonoBehaviour
{
    [SerializeField] private SpriteRenderer spriteRenderer = default;

    public void SetPosition(Vector2 position)
    {
        transform.position = position;
    }


    public void SetSize(Vector2 size)
    {
        transform.localScale = size;
    }

    public void SetColor(Color color)
    {
        spriteRenderer.color = color; 
    }
}
