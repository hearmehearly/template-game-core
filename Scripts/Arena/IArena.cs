﻿using System;


namespace Core
{
    public interface IArena
    {
        void ShowArena(Action<CoreArenaResult> onHided, Action onShowed = null);

        void LoadObjects(int index);
        void LoadObjects(string headerName);
        void UnloadObjects();
    }
}