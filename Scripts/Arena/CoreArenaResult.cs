﻿namespace Core
{
    public enum BattleResult
    {
        None = 0,
        Lose = 1,
        Draw = 2,
        Win = 3,
        Replay = 4,
        Return = 5
    }

    public class CoreArenaResult : UnitResult
    {
        public BattleResult battleResult = default;

        public bool IsWin =>
            battleResult == BattleResult.Win;
    }
}
