﻿using System;
using UnityEngine;


namespace Core
{
    public abstract class ParallaxBackground : MonoBehaviour
    {
        [Serializable]
        private class Body
        {
            public Transform body = default;
            [NonSerialized]
            public Vector3 InitialPosition = default;
        }

        [Serializable]
        private class Layer
        {
            public float OffsetX = default;
            public float OffsetY = default;
            public Body[] Bodies = default;
        }


        [SerializeField]
        private Layer[] layers = default;

        private Vector3 layerPosition; // to avoid allocations in Update()


        protected abstract float Border { get; }
        protected abstract float TopMaxBorder { get; }
        protected abstract float OffsetX { get; }
        protected abstract float OffsetY { get; }


        private void Awake()
        {
            RefreshInitialValues();
        }


        private void Update()
        {
            foreach (Layer layer in layers)
            {
                float layerDistanceX = Border * layer.OffsetX;
                float layerDistanceY = TopMaxBorder * layer.OffsetY;

                foreach (var body in layer.Bodies)
                {
                    layerPosition = body.body.position;
                    layerPosition.x = body.InitialPosition.x + OffsetX * layerDistanceX;
                    layerPosition.y = body.InitialPosition.y + OffsetY * layerDistanceY;

                    body.body.position = layerPosition;
                }
            }
        }


        public void RefreshInitialValues()
        {
            foreach (Layer layer in layers)
            {
                foreach (var body in layer.Bodies)
                {
                    body.InitialPosition = body.body.position;
                }
            }
        }
    }
}
