﻿using UnityEngine;


namespace Core.Proposal
{
    public abstract class RewardController
    {
        #region Fields

        private readonly string uallowKey;
        private readonly string uaDeltaLevelsKey;

        #endregion



        #region Class lifecycle

        public RewardController(string _uallowKey, string _uaDeltaLevelsKey)
        {
            uallowKey = _uallowKey;
            uaDeltaLevelsKey = _uaDeltaLevelsKey;
        }


        #endregion



        #region Properties

        public bool AllowToPropose => IsUaAllowDataSetted ? UaAllow : AbAllowToPropose;

        protected int ActualCompletedDeltaCounterLevels => IsUaLevelDataSetted ? UaLevelsDeltaCount : AbLevelsDeltaCount;


        public int UaLevelsDeltaCount
        {
            get => CustomPlayerPrefs.GetInt(uaDeltaLevelsKey, 0);
            set => CustomPlayerPrefs.SetInt(uaDeltaLevelsKey, value);
        }


        public bool UaAllow
        {
            get => CustomPlayerPrefs.GetBool(uallowKey, true);
            set => CustomPlayerPrefs.SetBool(uallowKey, value);
        }


        protected abstract bool AbAllowToPropose { get; }

        protected abstract int AbLevelsDeltaCount { get; }

        private bool IsUaLevelDataSetted => PlayerPrefs.HasKey(uaDeltaLevelsKey);

        private bool IsUaAllowDataSetted => PlayerPrefs.HasKey(uallowKey);

        #endregion



        #region Methods

        public void ClearUaLevelsData() => CustomPlayerPrefs.DeleteKey(uaDeltaLevelsKey);

        public void ClearUaAllowData() => CustomPlayerPrefs.DeleteKey(uallowKey);

        #endregion
    }
}
