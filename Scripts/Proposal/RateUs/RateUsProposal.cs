﻿using System;
using UnityEngine;


namespace Core.Proposal
{
    public abstract class RateUsProposal
    {
        #region Fields
        
        public abstract string RateIOSUrl { get; }
        public abstract string RateAndroidUrl { get; }

        public const string RateUsProposingDataKey = "RateUsProposingDataKey";
        private const string WasRatedPrefsKey = "WasRatedPrefsKey";

        #endregion



        #region Properties

        private DateTime ProposeDate
        {
            get => CustomPlayerPrefs.GetDateTime(RateUsProposingDataKey, DateTime.MinValue);
            set => CustomPlayerPrefs.SetDateTime(RateUsProposingDataKey, value);
        }
        

        private bool IsAtLeastOneDayPassed => TimeUtility.IsAtLeastOneDayPassed(ProposeDate, DateTime.Now);
        
        private bool IsRated => CustomPlayerPrefs.GetBool(WasRatedPrefsKey, false);

        public virtual bool CanPropose
        {
            get
            {
                bool canPropose = true;

                canPropose &= IsAtLeastOneDayPassed;
                canPropose &= !IsRated;

                return canPropose;
            }
        }

        #endregion



        #region Methods

        public void Propose(Action onProposed)
        {
            if (CanPropose)
            {
                ProposeDate = DateTime.Now;
                ShowPopup();
                RateApp();

                onProposed?.Invoke();
            }
        }

        protected abstract void ShowPopup();


        public void RateRequset()
        {
#if UNITY_IOS
                Application.OpenURL(RateIOSUrl);
#elif UNITY_ANDROID
                Application.OpenURL(RateAndroidUrl);
#endif

            RateApp();
        }

        private void RateApp() => CustomPlayerPrefs.SetBool(WasRatedPrefsKey, true);

#endregion
    }
}
