﻿namespace Core.Levels
{
    public enum LevelResult
    {
        None            = 0,
        Complete        = 1,
        Lose            = 2,
        Leave           = 3,
        Reload          = 4,
        Skip            = 5
    }
}
