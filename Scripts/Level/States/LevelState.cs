﻿namespace Core.Levels
{
    public enum LevelState
    {
        None                = 0,
        Initialized         = 1,
        Playing             = 2,
        AllTargetsHitted    = 3,
        OutOfAmmo           = 4,
        EndPlaying          = 5,
        Tutorial            = 6,
        FriendlyDeath       = 7,
        StageChanging       = 8,
        Paused              = 9,
        WaitingActionAfterAds  = 10 // TODO: hot fix. On lose screen we finish already finished level.
    }
}
