﻿using System;


namespace Core
{
    public abstract class BaseLevelControllers
    {
        protected ILevelController[] controllers;


        public BaseLevelControllers()
        {
        }


        public void Initialize()
        {
            foreach (var controller in controllers)
            {
                controller.Initialize();
            }
        }


        public void Deinitialize()
        {
            foreach (var controller in controllers)
            {
                controller.Deinitialize();
            }
        }


        public T FindLevelController<T>() where T : class, ILevelController
        {
            ILevelController result = Array.Find(controllers, e => e.GetType() == typeof(T));

            return result as T;
        }
    }
}
