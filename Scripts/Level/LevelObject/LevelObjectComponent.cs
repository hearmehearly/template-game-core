﻿namespace Core.Levels
{
    public abstract class LevelObjectComponent
    {
        public abstract void Enable();

        public abstract void Disable();
    }
}
