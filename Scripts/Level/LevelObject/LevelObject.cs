using System;
using UnityEngine;


namespace Core.Levels
{
    public class LevelObject : MonoBehaviour
    {
        public event Action<LevelObject> OnGameFinished;
        

        [Header("LevelObject")]
        [SerializeField] private int index = -1;

        private RigidbodyType2D defaultType;
        
        private Collider2D[] objectColliders;
        private Renderer[] renderers;
        

        public int Index => index;
        

        public LevelObjectData CurrentData { get; private set; }
        

        public Rigidbody2D Rigidbody2D { get; set; }


        public bool IsPrepareForCome { get; private set; }


        protected Renderer[] Renderers =>
            renderers = renderers ?? GetComponentsInChildren<Renderer>();


        private Collider2D[] ObjectColliders =>
            objectColliders = objectColliders ?? GetComponentsInChildren<Collider2D>();
        
        
        protected virtual void Awake() =>
            GetPhysicalBody();
        
        
        public virtual void SetData(LevelObjectData data)
        {
            if (IsPrepareForCome)
            {
                return;
            }

            transform.position = data.position;
            transform.eulerAngles = data.rotation;

            CurrentData = data;
        }
        

        public virtual void StartGame(Transform levelTransform)
        {
            if (Rigidbody2D != null)
            {
                Rigidbody2D.simulated = true;

                if (Rigidbody2D.bodyType != RigidbodyType2D.Static)
                {
                    Rigidbody2D.velocity = Vector3.zero;
                    Rigidbody2D.angularVelocity = 0.0f;
                }
            }

            SetCollidersEnabled(true);
        }


        public virtual void FinishGame()
        {
            if (Rigidbody2D != null)
            {
                if (Rigidbody2D.bodyType != RigidbodyType2D.Static)
                {
                    Rigidbody2D.velocity = Vector3.zero;
                    Rigidbody2D.angularVelocity = 0.0f;
                }
                
                Rigidbody2D.Sleep();
            }
            
            OnGameFinished?.Invoke(this);
        }


        public bool EqualData(LevelObjectData data) => CurrentData == data;


        protected virtual void GetPhysicalBody()
        {
            Rigidbody2D = GetComponent<Rigidbody2D>();

            if (Rigidbody2D != null)
            {
                defaultType = Rigidbody2D.bodyType;
            }
        }

        public virtual void PreSetData() { }
        
        
        private void SetCollidersEnabled(bool enable)
        {
            foreach (var c in ObjectColliders)
            {
                c.enabled = enable;
            }
        }


        private void SetRigidbodySimulated(bool simulated)
        {
            if (Rigidbody2D != null && !IsPrepareForCome)
            {
                Rigidbody2D.simulated = simulated;
            }
        }
    }
}
