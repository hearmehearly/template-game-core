﻿using UnityEngine;


namespace Core.Levels
{
    public abstract class ComponentLevelObject : LevelObject
    {
        #region Override methods

        public override void SetData(LevelObjectData data)
        {
            base.SetData(data);

            InitializeComponents();
        }

        public override void StartGame(Transform levelTransform)
        {
            base.StartGame(levelTransform);

            EnableComponents();
        }


        public override void FinishGame()
        {
            DisableComponents();

            base.FinishGame();
        }

        #endregion



        #region Abstract methods

        protected abstract void InitializeComponents();


        protected abstract void EnableComponents();


        protected abstract void DisableComponents();

        #endregion
    }
}
