﻿using System;
using System.Collections.Generic;
using UnityEngine;


namespace Core.Levels
{
    [Serializable]
    public class LevelObjectData
    {
        [Header("Common data")]
        public int index = default;

        public Vector3 position = default;
        public Vector3 rotation = default;
        public string additionalInfo = default;
        

        public LevelObjectData Copy()
        {
            LevelObjectData result = (LevelObjectData)MemberwiseClone();

            return result;
        }


        public LevelObjectData(){}


        public LevelObjectData(int index,
                               Vector3 position, 
                               Vector3 rotation, 
                               string additionalInfo)
        {
            this.index = index;
            this.position = position;
            this.rotation = rotation;
            this.additionalInfo = additionalInfo;
        }
        
    }
}

