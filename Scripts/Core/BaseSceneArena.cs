﻿using System;
using UnityEngine;


namespace Core
{
    public abstract class BaseSceneArena : MonoBehaviour
    {
        private Action onFinishArena;
        private CoreArenaResult arenaResult;

        protected abstract string LevelHeaderNameToLoad { get; }


        public virtual void Play(IArena arenaToPlay, Action onFinish)
        {
            onFinishArena = onFinish;

            OnPreShow();
            arenaToPlay.ShowArena(OnFinishArena);
            arenaToPlay.UnloadObjects();
            arenaToPlay.LoadObjects(LevelHeaderNameToLoad);
        }


        private void OnFinishArena(CoreArenaResult result)
        {
            arenaResult = result;
            OnFinish(arenaResult);
        }
        

        protected virtual void OnFinish(CoreArenaResult result)
        {
            onFinishArena();
        }


        protected virtual void OnPreShow() { }
    }
}
