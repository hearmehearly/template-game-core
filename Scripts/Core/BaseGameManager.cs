﻿using UnityEngine;
using UnityEngine.EventSystems;


namespace Core
{
    public abstract class BaseGameManager : SingletonMonoBehaviour<BaseGameManager>
    {
        private IArena arena;


        public GamePause GamePause { get; private set; }
        public GameTime GameTime { get; private set; }


        protected abstract bool ShouldStartWithArena { get; }

        protected override void Awake()
        {
            base.Awake();

            Input.multiTouchEnabled = true;
        }


        private void Start()
        {
            GamePause = new GamePause();
            GameTime = new GameTime();
            
            StartGame();
        }


        protected virtual void PreSetupGame()
        {
            GameTime.Initialize();
            EventSystemController.SetupEventSystem(EventSystem.current);
        }


        protected virtual void StartGame()
        {
            PreSetupGame();

            DevContent.SetEnabled(false);

            if (ShouldStartWithArena)
            {
                PlayArena();
            }
            else
            {
                ShowScenePlayer();
            }
        }
        

        protected abstract IArena GetArena();

        protected abstract BaseSceneArena GetBaseSceneArena();

        protected abstract BaseScenePlayer GetBaseScenePlayer();


        protected virtual void PlayArena()
        {
            arena = GetArena();

            GetBaseSceneArena().Play(arena, () => ShowScenePlayer());
        }


        protected virtual void ShowScenePlayer() =>
            GetBaseScenePlayer().Show(() => PlayArena());
    }
}