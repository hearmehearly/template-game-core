﻿using System;
using UnityEngine;


namespace Core
{
    public abstract class BaseScenePlayer : MonoBehaviour
    {
        private Action onHidedCallback;

        public virtual void Show(Action OnHidedCallback)
        {
            onHidedCallback = OnHidedCallback;
        }


        protected virtual void OnMenuShowed()
        {

        }

        protected virtual void InvokeOnHidedCallback() =>
            onHidedCallback?.Invoke();
    }
}
