﻿using UnityEngine;


namespace Core.Utils
{
    public static class TransformUtility
    {
        private static Camera UiCamera;
        private static Camera gameCamera;


        public static void Initialize(Camera _UiCamera, Camera _gameCamera)
        {
            UiCamera = _UiCamera;
            gameCamera = _gameCamera;
        }


        public static Vector3 UiToWorldPosition(this Vector3 position)
        {
            Vector3 viewPortUIPosition = UiCamera.WorldToViewportPoint(position);
            Vector3 worldPosition = gameCamera.ViewportToWorldPoint(viewPortUIPosition);

            return worldPosition;
        }


        public static Vector3 WorldToUiPosition(this Vector3 position, Canvas canvas)
        {
            RectTransform CanvasRect = canvas.GetComponent<RectTransform>();

            Vector2 ViewportPosition = gameCamera.WorldToViewportPoint(position);
            Vector2 WorldObject_ScreenPosition = new Vector2(
            ((ViewportPosition.x * CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f)),
            ((ViewportPosition.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f)));


            return WorldObject_ScreenPosition;
        }
    }
}
