﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Core
{
    public static class AudioManager
    {
        public static bool IsSoundActive { get; set; } = true;
        public static bool IsMusicActive { get; set; } = true;
    }
}
