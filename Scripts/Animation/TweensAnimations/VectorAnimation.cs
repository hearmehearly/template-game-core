﻿
using System;
using DG.Tweening;
using DG.Tweening.Core;
using UnityEngine;


namespace Core
{
    [Serializable]
    public class VectorAnimation : CommonAnimation
    {
        #region Fields

        public Vector3 beginValue = default;
        public Vector3 endValue = default;

        #endregion



        #region Public methods

        public void Play(DOSetter<Vector3> setter, object handler, Action callback = null, bool isReversed = false, GameObject link = null)
        {
            Vector3 begin = isReversed ? endValue : beginValue;
            Vector3 end = isReversed ? beginValue : endValue;

            var tween = DOTween
                .To(() => begin, setter, end, duration)
                .SetDelay(delay)
                .SetEase(curve)
                .SetId(handler)
                .SetUpdate(shouldUseUnscaledDeltaTime)
                .OnComplete(() => callback?.Invoke());

            if (link != null)
            {
                tween.SetLink(link);
            }

            if (loop)
            {
                tween.SetLoops(-1, loopType);
            }
        }

        public float CalculateDuration(float speed) =>
            speed == 0.0f ? float.MaxValue : Vector3.Distance(beginValue, endValue) / speed;

                
        public void SetupBeginValue(Vector3 value) =>
            beginValue = value;


        public void SetupEndValue(Vector3 value) =>
            endValue = value;


        public void SetupData(VectorAnimation animation)
        {
            base.SetupData(animation);

            beginValue = animation.beginValue;
            endValue = animation.endValue;
        }

        #endregion
    }
}
