﻿using System;
using DG.Tweening;
using DG.Tweening.Core;
using UnityEngine;


namespace Core
{
    [Serializable]
    public class NumberAnimation : CommonAnimation
    {
        #region Fields

        public float beginValue = default;
        public float endValue = default;

        #endregion



        #region Public methods

        public void Play(DOSetter<float> setter, object handler, Action callback = null)
        {
            var tween = DOTween
                .To(() => beginValue, setter, endValue, duration)
                .SetDelay(delay)
                .SetEase(curve)
                .SetId(handler)
                .SetUpdate(shouldUseUnscaledDeltaTime)
                .OnComplete(() => callback?.Invoke());

            if (loop)
            {
                tween.SetLoops(-1, loopType);
            }
        }


        public void SetupData(FactorAnimation animation)
        {
            base.SetupData(animation);

            beginValue = animation.beginValue;
            endValue = animation.endValue;
        }

        #endregion
    }
}
