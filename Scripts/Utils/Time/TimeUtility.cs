﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;


namespace Core
{
    public static class TimeUtility
    {
        #region Fields

        private static readonly List<object> handlers = new List<object>();

        private static readonly float defaultTimeScale;
        private static readonly float defaultFixedStep;

        #endregion



        #region Ctor

        static TimeUtility()
        {
            defaultTimeScale = Time.timeScale;
            defaultFixedStep = Time.fixedDeltaTime;
        }

        #endregion



        #region Methods

        public static bool IsAtLeastOneDayPassed(DateTime from, DateTime to) => to.Date.Subtract(from.Date).Days > 0;

        public static bool IsEnoughSecondsPassed(DateTime from, DateTime to, double seconds) => to.Subtract(from).TotalSeconds >= seconds;

        public static void Clear()
        {
            handlers.ForEach(o => DOTween.Kill(o, true));

            Time.timeScale = defaultTimeScale;
            Time.fixedDeltaTime = defaultFixedStep;
        }

        public static void PlaySlowmo(float endTimeScale,
                                      float duration,
                                      AnimationCurve curve,
                                      object handler,
                                      Action onComplete)
        {
            DOTween.Kill(handler, true);

            handlers.Add(handler);

            Tween tween = DOTween.To(() => Time.timeScale,
                                     value =>
                                     {
                                         Time.timeScale = value;
                                         Time.fixedDeltaTime = defaultFixedStep * value;

                                         Debug.Log(Time.timeScale);
                                     },
                                     endTimeScale,
                                     duration)
                            .OnComplete(() => onComplete?.Invoke())
                            .SetId(handler);

            if (curve != null)
            {
                tween.SetEase(curve);
            }

            tween.Play();
        }
        
        #endregion
    }
}

