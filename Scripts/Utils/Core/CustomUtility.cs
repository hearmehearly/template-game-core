﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Random = UnityEngine.Random;


public static class CustomUtility
{
    public static T RandomValue<T>(this T[] soure) where T : IConvertible   //enum
    {
        if (!typeof(T).IsEnum)
            throw new ArgumentException("T must be an enumerated type");

        return (T)soure.GetValue(Random.Range(0, soure.Length - 1));
    }


    public static string ToTotalMMSS(this TimeSpan timeSpan, string mask = "{0:D2}:{1:D2}")
    {
        string result = timeSpan.TotalSeconds > 0 ?
            string.Format(mask, (int)timeSpan.TotalMinutes, timeSpan.Seconds) :
            string.Format(mask, 0, 0);

        return result;
    }


    public static string ToTotalHHMMSS(this TimeSpan timeSpan, string mask = "{0:D2}:{1:D2}:{2:D2}")
    {
        string result = timeSpan.TotalSeconds > 0 ?
            string.Format(mask, (int)timeSpan.TotalHours, timeSpan.Minutes, timeSpan.Seconds) :
            string.Format(mask, 0, 0, 0);

        return result;
    }


    public static string ToTotalHHMM(this TimeSpan timeSpan, string mask = "{0:D2}:{1:D2}")
    {
        string result = timeSpan.TotalSeconds > 0 ?
            string.Format(mask, (int)timeSpan.TotalHours, timeSpan.Minutes) :
            string.Format(mask, 0, 0);

        return result;
    }

    public static bool IsNullOrEmpty<T>(this LinkedList<T> list)
    {
        return list == null || list.Count == 0;
    }


    public static void AddRangeAsLast<T>(this LinkedList<T> list, IEnumerable<T> range)
    {
        foreach (T item in range)
        {
            list.AddLast(item);
        }
    }


    /// <summary>
    /// Perform a deep Copy of the object.
    /// </summary>
    /// <typeparam name="T">The type of object being copied.</typeparam>
    /// <param name="source">The object instance to copy.</param>
    /// <returns>The copied object.</returns>
    public static T Clone<T>(T source)
    {
        if (!typeof(T).IsSerializable)
        {
            throw new ArgumentException("The type must be serializable.", nameof(source));
        }

        // Don't serialize a null object, simply return the default for that object
        if (Object.ReferenceEquals(source, null))
        {
            return default(T);
        }

        IFormatter formatter = new BinaryFormatter();
        Stream stream = new MemoryStream();
        using (stream)
        {
            formatter.Serialize(stream, source);
            stream.Seek(0, SeekOrigin.Begin);
            return (T)formatter.Deserialize(stream);
        }
    }


    public static bool IsNull(this object target)
    {
        if (target is UnityEngine.Object)
        {
            return target as UnityEngine.Object;
        }

        return target == null;
    }

}
