﻿using UnityEngine;


public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{

    public static T Instance
    {
        get;
        private set;
    }


    protected virtual void Awake()
    {
        Instance = GetComponent<T>();
    }
}
