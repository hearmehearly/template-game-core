﻿using UnityEngine;


public class GameTime
{ 
    public void Initialize()
    {
        MonoBehaviourLifecycle.OnUpdate += MonoBehaviourLifecycle_OnUpdate;
    }


    public void Deinitialize()
    {
        MonoBehaviourLifecycle.OnUpdate -= MonoBehaviourLifecycle_OnUpdate;
    }


    private void MonoBehaviourLifecycle_OnUpdate(float deltaTime)
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Time.timeScale = 0.1f;
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            Time.timeScale = 0.4f;
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            Time.timeScale = 0.7f;
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            Time.timeScale = 1.0f;
        }
    }
}

