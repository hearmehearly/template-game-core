﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class SchedulerTask
{
    public object Target;
    public Action Method;

    public float Interval;
    public float Elapsed;
    public bool AutoRemove;
    public bool Paused;
    public bool IsNeedUnscaledDeltaTime;


    public SchedulerTask(object target, Action name, float interval, bool isNeedUseUnscaledDeltaTime) :
        this(target, name, interval, isNeedUseUnscaledDeltaTime, false)
    { }


    public SchedulerTask(object target, Action name, float interval, bool isNeedUseUnscaledDeltaTime, bool autoRemove)
    {
        Target = target;
        Method = name;
        Interval = interval;
        AutoRemove = autoRemove;
        Elapsed = 0;
        Paused = false;
        IsNeedUnscaledDeltaTime = isNeedUseUnscaledDeltaTime;
    }


    public void CustomUpdate(float deltaTime)
    {
        if (Target != null &&
            Method != null &&
            !Paused)
        {
            Elapsed += deltaTime;
            if (Elapsed >= Interval)
            {
                Elapsed -= Interval;
            }
            else
            {
                return;
            }
            Method();
            if (AutoRemove)
            {
                TaskInvoker.Instance.UnscheduleTask(this);
            }
        }
    }


    public float GetRemainingTime()
    {
        return Mathf.Clamp(Interval - Elapsed, 0f, Interval);
    }
}

public class TaskInvoker : MonoBehaviour
{
    readonly LinkedList<SchedulerTask> schedulers = new LinkedList<SchedulerTask>();
    readonly LinkedList<SchedulerTask> addList = new LinkedList<SchedulerTask>();
    readonly LinkedList<SchedulerTask> removeList = new LinkedList<SchedulerTask>();

    public static TaskInvoker Instance { get; private set; }


    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void Initialize()
    {
        GameObject go = new GameObject("Monobehavior TaskInvoker");
        Instance = go.AddComponent<TaskInvoker>();
        DontDestroyOnLoad(go);
    }


    void Update()
    {
        if (!addList.IsNullOrEmpty())
        {
            schedulers.AddRangeAsLast(addList);
            addList.Clear();
        }

        if (!removeList.IsNullOrEmpty())
        {
            foreach (SchedulerTask task in removeList)
            {
                schedulers.Remove(task);
            }

            removeList.Clear();
        }

        float deltaTime = Time.deltaTime;
        float unscaledDeltaTime = Time.unscaledDeltaTime;

        foreach (SchedulerTask task in schedulers)
        {
            float currentDeltaTime = (task.IsNeedUnscaledDeltaTime) ? (unscaledDeltaTime) : (deltaTime);
            task.CustomUpdate(currentDeltaTime);
        }
    }

    public void ScheduleMethod(object target, Action selector, float pInterval, bool isNeedUnscaledDeltaTime = false)
    {
        bool isExist = false;
        foreach (var task in schedulers)
        {
            if ((task.Target == target) && (task.Method == selector))
            {
                task.Interval = pInterval;
                task.Elapsed = 0;
                isExist = true;
            }
        }
        foreach (var task in addList)
        {
            if ((task.Target == target) && (task.Method == selector))
            {
                task.Interval = pInterval;
                task.Elapsed = 0;
                isExist = true;
            }
        }
        if (!isExist)
        {
            var task = new SchedulerTask(target, selector, pInterval, isNeedUnscaledDeltaTime);
            addList.AddLast(task);
        }
    }

    public void ScheduleMethod(object target, Action selector)
    {
        ScheduleMethod(target, selector, 0.0f);
    }


    public SchedulerTask CallMethodWithDelay(object target, Action selector, float delay, bool isNeedUnscaledDeltaTime = false)
    {
        var task = new SchedulerTask(target, selector, delay, isNeedUnscaledDeltaTime, true);
        addList.AddLast(task);

        return task;
    }


    public void UnscheduleAllMethodForTarget(object target)
    {
        foreach (var task in schedulers)
        {
            if (task.Target == target)
            {
                removeList.AddLast(task);
            }
        }
        foreach (var task in addList)
        {
            if (task.Target == target)
            {
                removeList.AddLast(task);
            }
        }
    }


    public void UnscheduleMethod(object target, Action selector)
    {
        foreach (var task in schedulers)
        {
            if ((task.Target == target) && (task.Method == selector))
            {
                removeList.AddLast(task);
            }
        }
        foreach (var task in addList)
        {
            if ((task.Target == target) && (task.Method == selector))
            {
                removeList.AddLast(task);
            }
        }
    }


    public void UnscheduleTask(SchedulerTask unTask)
    {
        removeList.AddLast(unTask);
    }


    public void PauseMethod(object target, Action selector)
    {
        foreach (var task in schedulers)
        {
            if ((task.Target == target) && (task.Method == selector))
            {
                task.Paused = true;
            }
        }
        foreach (SchedulerTask task in addList)
        {
            if ((task.Target == target) && (task.Method == selector))
            {
                task.Paused = true;
            }
        }
    }


    public void UnpauseMethod(object target, Action selector)
    {
        foreach (var task in schedulers)
        {
            if ((task.Target == target) && (task.Method == selector))
            {
                task.Paused = false;
            }
        }
        foreach (var task in addList)
        {
            if ((task.Target == target) && (task.Method == selector))
            {
                task.Paused = false;
            }
        }
    }


    public void PauseAllMethodForTarget(object target)
    {
        foreach (var task in schedulers)
        {
            if (task.Target == target)
            {
                task.Paused = true;
            }
        }
        foreach (var task in addList)
        {
            if (task.Target == target)
            {
                task.Paused = true;
            }
        }
    }


    public void UnpauseAllMethodForTarget(object target)
    {
        foreach (var task in schedulers)
        {
            if (task.Target == target)
            {
                task.Paused = false;
            }
        }
        foreach (var task in addList)
        {
            if (task.Target == target)
            {
                task.Paused = false;
            }
        }
    }
}
