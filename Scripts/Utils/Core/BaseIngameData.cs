﻿using System;
using UnityEngine;


namespace Core
{
    [Serializable]
    public abstract class BaseSettingsData { }

    /// <summary>
    /// Requare custom serialization as no native generic serialization exists
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [CreateAssetMenu]
    public class BaseIngameData<T> : ScriptableObject where T : BaseSettingsData
    {
        public static readonly ResourceAsset<BaseIngameData<T>> asset = new ResourceAsset<BaseIngameData<T>>("Game/IngameData");


        public T Settings = default;
    }
}