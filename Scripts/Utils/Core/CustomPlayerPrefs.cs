﻿using System;
using UnityEngine;


public static class CustomPlayerPrefs
{
    private const int BOOL_TRUE_INT_VALUE = 1;
    private const int BOOL_FALSE_INT_VALUE = 0;


    public static void DeleteKey(string key) =>
        PlayerPrefs.DeleteKey(key);

    public static bool HasKey(string key)
        => PlayerPrefs.HasKey(key);


    public static int GetInt(string key) =>
        GetInt(key, default);


    public static int GetInt(string key, int defaultValue) =>
        PlayerPrefs.GetInt(key, defaultValue);


    public static void SetInt(string key, int value, bool isSaveImmediately = false)
    {
        PlayerPrefs.SetFloat(key, value);

        if (isSaveImmediately)
        {
            PlayerPrefs.Save();
        }
    }


    public static float GetFloat(string key) =>
        GetFloat(key, default);


    public static float GetFloat(string key, float defaultValue) =>
        PlayerPrefs.GetFloat(key, defaultValue);


    public static void SetFloat(string key, float value, bool isSaveImmediately = false)
    {
        PlayerPrefs.SetFloat(key, value);

        if (isSaveImmediately)
        {
            PlayerPrefs.Save();
        }
    }


    public static void SetBool(string key, bool value, bool isSaveImmediately = false)
    {
        int targetValue = (value) ? (BOOL_TRUE_INT_VALUE) : (BOOL_FALSE_INT_VALUE);
        PlayerPrefs.SetInt(key, targetValue);
    }


    public static bool GetBool(string key, bool defaultValue)
    {
        int currentDefaultValue = defaultValue ? BOOL_TRUE_INT_VALUE : BOOL_FALSE_INT_VALUE;
        return PlayerPrefs.GetInt(key, currentDefaultValue) == BOOL_TRUE_INT_VALUE;
    }


    public static bool GetBool(string key)
    {
        return PlayerPrefs.GetInt(key) == BOOL_TRUE_INT_VALUE;
    }


    public static void SetObjectValue<T>(string key, T value, bool saveImmediately = false)
        where T : class
    {
        string objectValue = (value == null) ? (string.Empty) : (JsonConvert.SerializeObject(value));

        SetString(key, objectValue, saveImmediately);
    }


    public static T GetObjectValue<T>(string key) where T : class
    {
        string savedObjectValue = GetString(key);

        return (string.IsNullOrEmpty(savedObjectValue))
            ? (null)
            : (JsonConvert.DeserializeObject<T>(savedObjectValue));
    }


    public static DateTime GetDateTime(string key) =>
        GetDateTime(key, default);


    public static DateTime GetDateTime(string key, DateTime defaultValue)
    {
        string savedString = PlayerPrefs.GetString(key);
        DateTime result = defaultValue;

        if (!string.IsNullOrEmpty(savedString))
        {
            long temp = Convert.ToInt64(savedString);
            result = DateTime.FromBinary(temp);
        }

        return result;
    }


    public static string GetString(string key)
    {
        return PlayerPrefs.GetString(key, "default_string");
    }


    public static void SetString(string key, string value, bool isSaveImmediately = false)
    {
        PlayerPrefs.SetString(key, value);

        if (isSaveImmediately)
        {
            PlayerPrefs.Save();
        }
    }


    public static void SetDateTime(string key, DateTime value, bool isSaveImmediately = false)
    {
        PlayerPrefs.SetString(key, value.ToBinary().ToString());

        if (isSaveImmediately)
        {
            PlayerPrefs.Save();
        }
    }
}
