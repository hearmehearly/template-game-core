﻿using UnityEngine.Events;
using UnityEngine.EventSystems;


namespace UnityEngine.UI
{
    [System.Serializable]
    public class onClickUp : UnityEvent<float> { }



    public class UIButton : Button
    {
        bool isPressed;
        float holdDuration;

        //
        // Summary:
        //     UnityEvent that is triggered when the Button is pressed once.
        public UnityEvent onPressed { get; set; } = new UnityEvent();

        public onClickUp onClickUp { get; set; } = new onClickUp();

        public override void OnPointerDown(PointerEventData data)
        {
            base.OnPointerDown(data);

            holdDuration = 0.0f;
            isPressed = true;

            onPressed?.Invoke();
        }

        public override void OnPointerUp(PointerEventData data)
        {
            base.OnPointerUp(data);

            onClickUp?.Invoke(holdDuration);

            holdDuration = 0.0f;
            isPressed = false;
        }

        private void Update()
        {
            if (isPressed)
            {
                holdDuration += Time.deltaTime;
            }
        }
    }
}
