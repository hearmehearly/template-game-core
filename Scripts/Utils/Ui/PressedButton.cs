﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.Events;

public class PressedButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public event Action OnPressed;

    public UnityEvent onPressedDown { get; set; } = new UnityEvent();

    public UnityEvent onPressedUp { get; set; } = new UnityEvent();

    bool isPressed;



    private void Update()
    {
        if (isPressed)
        {
            OnPressed?.Invoke();
        }
    }


    public void OnPointerDown(PointerEventData data)
    {
        isPressed = true;

        onPressedDown?.Invoke();
    }

    public void OnPointerUp(PointerEventData data)
    {
        isPressed = false;

        onPressedUp?.Invoke();
    }
}
