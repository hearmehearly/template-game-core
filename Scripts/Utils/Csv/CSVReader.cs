﻿using System;
using System.IO;


namespace Core.Utils
{
    public static class CSVReader
    {
        public static void ReadCsvByRows(string csvText, Action<string[]> onRowReadCallback, int rowToSkip = 1)
        {
            using (var reader = new StringReader(csvText))
            {
                for (int i = 0; i < rowToSkip; i++)
                {
                    reader.ReadLine(); // skip header
                }

                string line = reader.ReadLine();

                while (!string.IsNullOrEmpty(line))
                {
                    string[] rowValues = CSVParseUtility.ParseRowsToArray(line);
                    onRowReadCallback?.Invoke(rowValues);

                    line = reader.ReadLine();
                }
            }
        }
    }
}
