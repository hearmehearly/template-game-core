﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;


namespace Core.Utils
{
    public static class CSVDownloader
    {
        private const string UrlGoogleCsvExportFormat = "https://docs.google.com/spreadsheets/d/{0}/export?format=csv&gid={1}";

        /// <summary>
        /// Download google sheet as csv by id in to local path (under Assets)
        /// </summary>
        public static async Task DownloadDataAsync(string googleSheedDocId, string saveDataPath, int gid = 0, Action onCompleted = null)
        {
            string url = string.Format(UrlGoogleCsvExportFormat, googleSheedDocId, gid);
            string saveFilePath = string.Concat(Application.dataPath, saveDataPath);

            //TODO: maybe uncomment if access denied
            //  File.SetAttributes(resultFile, FileAttributes.Normal);
            DownloadHandlerFile downloadHandler = new DownloadHandlerFile(saveFilePath);

            var webRequest = new UnityWebRequest(url)
            {
                method = UnityWebRequest.kHttpVerbGET,
                downloadHandler = downloadHandler
            };

            var requestAsyncOperation = webRequest.SendWebRequest();

            while (!requestAsyncOperation.isDone)
            {
                await Task.Delay(100);
            }

            onCompleted?.Invoke();
        }


        /// <summary>
        /// Read google sheet as csv by id and gid
        /// Params: googleSheedDocId which is 13ZW00Ea4Cx4LdH_OSbqM2I_IPHy19vXcB9V_CCR9FWE
        /// Params: gID which is 831622442
        /// </summary>
        public static async Task ReadDataAsync(string googleSheedDocId, int gid = 0, Action<string> onCompleted = null)
        {
            string url = string.Format(UrlGoogleCsvExportFormat, googleSheedDocId, gid);

            var webRequest = new UnityWebRequest(url)
            {
                method = UnityWebRequest.kHttpVerbGET,
                downloadHandler = new DownloadHandlerBuffer()
            };

            var requestAsyncOperation = webRequest.SendWebRequest();
            try
            {
                while (!requestAsyncOperation.isDone)
                {
                    await Task.Delay(100);
                }
            }
            catch
            {
                webRequest.Abort();
            }

            onCompleted?.Invoke(webRequest.downloadHandler.text);
        }
    }
}
