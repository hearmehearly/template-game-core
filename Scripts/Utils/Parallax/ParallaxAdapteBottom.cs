﻿using UnityEngine;


[RequireComponent(typeof(SpriteRenderer))]
public class ParallaxAdapteBottom : MonoBehaviour
{
    [SerializeField]
    private float maxTopValue = 0f;



    private void Awake()
    {
        transform.position = new Vector3(transform.position.x, -Camera.main.orthographicSize, transform.position.z);

        if (transform.position.y > maxTopValue)
        {
            transform.position = new Vector3(transform.position.x, maxTopValue, transform.position.z);
        }
    }
}
