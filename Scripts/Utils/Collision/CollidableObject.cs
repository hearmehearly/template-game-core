﻿using System;
using UnityEngine;


public class CollidableObject : MonoBehaviour
{
    public enum Type
    {
        None                    = 0,
        Player                  = 2,
        Ground                  = 3,
        HitCollider             = 4,
        NPCFighter              = 5,
        HitBullet               = 6,
        DeathRing               = 7,
        ExecutionerAxe          = 8,
        ExecutionBulletRalph    = 9,
        AbilityHitZone          = 10,
        WitchDarkAura           = 11,
        WitchCursedBlood        = 12,

        FighterPhysics           = 13,
        GroundWall               = 14,
        SmokeBomb                = 15,
        WallSide                 = 16
    }

    /// <param name="Collision2D">An integer.</param>
    /// <param name="Type">An integer.</param>
    public event Action<Collision2D, Type> OnCustomCollisionEnter2D;
    public event Action<Collider2D, Type> OnCustomTriggerEnter2D;
    public event Action<Collider2D, Type> OnCustomTriggerExit2D;

    [SerializeField] Type type = Type.None;



    public Type CurrentType => type;



    void OnCollisionEnter2D(Collision2D collision)
    {
        Type anotherObjectType = Type.None;
        CollidableObject anotherObject = collision.gameObject.GetComponent<CollidableObject>();

        if (anotherObject)
        {
            anotherObjectType = anotherObject.type;
        }

        OnCustomCollisionEnter2D?.Invoke(collision, anotherObjectType);
    }


    void OnTriggerEnter2D(Collider2D collision)
    {
        Type anotherObjectType = Type.None;
        CollidableObject anotherObject = collision.gameObject.GetComponent<CollidableObject>();

        if (anotherObject != null)
        {
            anotherObjectType = anotherObject.type;
        }
        
        OnCustomTriggerEnter2D?.Invoke(collision, anotherObjectType);
    }


    void OnTriggerExit2D(Collider2D collision)
    {
        Type anotherObjectType = Type.None;
        CollidableObject anotherObject = collision.gameObject.GetComponent<CollidableObject>();

        if (anotherObject != null)
        {
            anotherObjectType = anotherObject.type;
        }

        OnCustomTriggerExit2D?.Invoke(collision, anotherObjectType);
    }
}
