﻿using System;
using UnityEngine;


namespace Core
{
    public class MouseInputObjectMonitor : MonoBehaviour
    {
        public event Action<object> OnMouseDownAction;
        public event Action<object> OnMouseUpAction;

        protected virtual void OnMouseDown()
        {
            OnMouseDownAction?.Invoke(this);
        }


        protected virtual void OnMouseUp()
        {
            OnMouseUpAction?.Invoke(this);
        }
    }
}

