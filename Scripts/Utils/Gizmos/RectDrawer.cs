﻿using UnityEngine;


public class RectDrawer : MonoBehaviour
{
    public Rect rect = default;
    public Color rectColor = default;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = rectColor;

        Gizmos.DrawCube(rect.center, rect.size);
    }
}
