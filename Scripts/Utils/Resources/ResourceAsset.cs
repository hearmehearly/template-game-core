﻿using UnityEngine;

namespace Core 
{
    public class ResourceAsset<T> where T : Object
    {
        private readonly string path;
        private T asset;

        public ResourceAsset(string path)
        {
            this.path = path;
        }


        public ResourceAsset(string path, string name)
        {
            this.path = string.Concat(path, name);
        }


        public T Value
        {
            get
            {
                asset = asset ?? Resources.Load<T>(path);
                return asset as T;
            }
        }
    }
}
