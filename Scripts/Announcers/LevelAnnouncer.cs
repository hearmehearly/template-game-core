﻿using System;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;


namespace Core
{
    public class LevelAnnouncer : MonoBehaviour, ILifecycleObject
    {
        [Serializable]
        public class Data
        {
            public VectorAnimation moveAnimation = default;
            public FactorAnimation alphaAnimation = default;
            public VectorAnimation scaleAnimation = default;

        }

        [SerializeField] private Graphic[] graphics = default;

        [SerializeField] private TMP_Text text = default;
        [SerializeField] private Image image = default;

        private Data data;


        public void SetupData(Data _data) =>
            data = _data;


        public void SetupData(VectorAnimation _moveAnimation,
                              FactorAnimation _alphaAnimation = default,
                              VectorAnimation _scaleAnimation = default)
        {
            data = new Data
            {
                moveAnimation = _moveAnimation,
                alphaAnimation = _alphaAnimation,
                scaleAnimation = _scaleAnimation
            };
        }


        public void SetupText(string _text) =>
            text.text = _text;


        public void SetupImage(Sprite _sprite)
        {
            image.sprite = _sprite;
            image.SetNativeSize();
        }


        public void Initialize()
        {
            
        }


        public void Deinitialize()
        {
            gameObject.SetActive(false);
            DOTween.Kill(this);
        }


        public void Show(Vector3 beginPosition, Vector3 offset)
        {
            if (data == null)
            {
                var commonData = LevelAnnouncerSettings.asset.Value.announcersCommonData;
                SetupData(commonData);
            }

            DOTween.Complete(this);

            transform.position = beginPosition;

            data.moveAnimation.beginValue = beginPosition;
            data.moveAnimation.endValue = beginPosition + offset;

            data.moveAnimation.Play((value) => transform.position = value, this);

            float startAlphaValue = data.alphaAnimation == null ? 1.0f : 0.0f;

            foreach (var graphic in graphics)
            {
                graphic.color = graphic.color.SetA(startAlphaValue);
            }

            data.alphaAnimation?.Play((value) => 
            {
                foreach (var graphic in graphics)
                {
                    graphic.color = graphic.color.SetA(value);
                }
            }, this);
            data.scaleAnimation?.Play((value) => transform.localScale = value, this);
        }
    }
}
