﻿using System.Collections.Generic;
using UnityEngine;


namespace Core
{
    public abstract class LevelAnnouncerController : ILevelController
    {
        protected readonly List<LevelAnnouncer> announcers;



        public LevelAnnouncerController()
        {
            announcers = new List<LevelAnnouncer>();
        }

        public virtual void Initialize()
        {
            foreach (var announcer in announcers)
            {
                announcer.Deinitialize();
            }

            announcers.Clear();
        }


        public virtual void Deinitialize()
        {
        }


        protected abstract void DestroyLevelAnnouncer(LevelAnnouncer announcerToDestroy);


        protected abstract LevelAnnouncer CreateLevelAnnouncer(LevelAnnouncer announcerPrefab);



        protected void PlayAnnouncer(LevelAnnouncerType announcerType, Vector3 startPosition, Vector3 offset, Quaternion rotation)
        {
            LevelAnnouncer announcerPrefab = LevelAnnouncerSettings.asset.Value.GetRandomLevelAnnouncer(announcerType);
            LevelAnnouncer announcer = CreateLevelAnnouncer(announcerPrefab);
            announcer.transform.rotation = rotation;

            announcer.Show(startPosition, offset);

            announcers.Add(announcer);
        }


    }
}
