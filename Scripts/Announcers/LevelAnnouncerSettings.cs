﻿using System;
using UnityEngine;


namespace Core
{
    public class LevelAnnouncerSettings : ScriptableObjectData<LevelAnnouncerSettings.Data, LevelAnnouncerType>
    {
        [Serializable]
        public class Data : ScriptableObjectBaseData<LevelAnnouncerType>
        {
            public LevelAnnouncer[] announcers = default;
        }


        public float levelFinishedAnnouncerDelay = default;

        public static readonly ResourceAsset<LevelAnnouncerSettings> asset = 
            new ResourceAsset<LevelAnnouncerSettings>("Game/Core/", nameof(LevelAnnouncerSettings));


        public LevelAnnouncer.Data announcersCommonData = default;
        public Vector3 announcersCommonOffset = default;


        public LevelAnnouncer GetRandomLevelAnnouncer(LevelAnnouncerType type)
        {
            var foundData = FindData(type);
            return foundData == null ? default : foundData.announcers.RandomObject();
        }

    }
}
