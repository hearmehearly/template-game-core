﻿namespace Core
{
    public enum LevelAnnouncerType 
    {
        None = 0,
        LevelCompleted = 1,
        EnemyDefeated = 2
    }
}
