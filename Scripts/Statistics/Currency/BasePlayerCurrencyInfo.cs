﻿using System;
using System.Linq;
using Core.Helpers;


namespace Core
{
    public class BasePlayerCurrencyInfo<T> where T : IConvertible
    {
        public static readonly T[] PlayerTypes = Enum.GetValues(typeof(T))
                                                                .Cast<T>()
                                                                .Where(e => !e.Equals(default))
                                                                .ToArray();

        public event Action<T, float> OnCurrencyCountChanged;
        public event Action<T, float> OnCurrencyAdded; 
        public event Action OnAnyCurrencyCountChanged;

        private const string CurrencyCountPrefix = "Currency_Count_Prefix";


        public float GetEarnedCurrency(T type)
        {
            string prefsKey = GetSavedPrefsKey(type);
            return CustomPlayerPrefs.GetFloat(prefsKey, 0);
        }

        public void AddCurrency(T type, float value)
        {
            float currentValue = GetEarnedCurrency(type);
            float valueToSet = currentValue + value;
            SetEarnedCurrency(type, valueToSet);

            OnCurrencyAdded?.Invoke(type, value);
        }


        public bool TryRemoveCurrency(T type, float value)
        {
            bool result = default;
            float currentValue = GetEarnedCurrency(type);

            if (currentValue >= value)
            {
                float valueToSet = currentValue - value;
                SetEarnedCurrency(type, valueToSet);

                result = true;
            }

            return result;
        }


        public string GetUiCurrencyText(T type)
        {
            float currentValue = GetEarnedCurrency(type);
            return currentValue.ToShortFormat();
        }


        private void SetEarnedCurrency(T type, float value)
        {
            float previousCount = GetEarnedCurrency(type);
            string prefsKey = GetSavedPrefsKey(type);

            CustomPlayerPrefs.SetFloat(prefsKey, value);

            float currencyDelta = value - previousCount;
            OnCurrencyCountChanged?.Invoke(type, currencyDelta);
            OnAnyCurrencyCountChanged?.Invoke();
        }


        private string GetSavedPrefsKey(T type)
        {
            string result = string.Concat(CurrencyCountPrefix, type);

            return result;
        }
    }
}
