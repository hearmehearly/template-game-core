﻿using System;


namespace Core
{
    [Serializable]
    public abstract class BasePlayerData
    {
    }


    public class BasePlayerInfo<T> : IInitializable where T : BasePlayerData, new()
    {
        private const string PlayerDataKey = "PlayerDataKey";
        
        protected T data;
        

        public virtual void Initialize()
        {
            data = CustomPlayerPrefs.GetObjectValue<T>(PlayerDataKey);

            if (data == null)
            {
                data = new T();

                OnFirstInfoCreated();
            };

            OnDataCreated();
            SaveData();
        }

        protected virtual void OnFirstInfoCreated() { }


        protected virtual void OnDataCreated() { }


        public void SaveData() => 
            CustomPlayerPrefs.SetObjectValue(PlayerDataKey, data);
    }
}
