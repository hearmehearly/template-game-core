﻿using System;


namespace Core
{
    public class StatisticsData
    {
        private const double TimeDifferenceToDetermineCheater = -5.0d;

        private const string IsFirstLaunchKey = "is_first_launch";
        private const string FirstLaunchTimeKey = "first_launch_time";

        private const string TotalVideoShowCountKey = "total_video_show_count";

        private const string MinutesInGameKey = "minutes_in_game";

        private const string InterstitialWatchCountKey = "interstitial_watch_count";
        private const string VideoWatchCountKey = "video_watch_count";

        private const string LastActiveDayKey = "last_active_day";

        private const string MatchesCountKey = "matches_count";
        private const string TodayMatchesCountKey = "today_matches_count";
        private const string TodayMatchesCountDateKey = "today_matches_count_date";

        private const string UniqueLevelsCountKey = "unique_levels_count";
        private const string MatchStartDateKey = "match_start_date";
        private const string IsNeedSendRetentionEventsKey = "is_need_send_retention_events";
        private const string CurrentDayReturnCountKey = "current_day_return_count";
        private const string CurrentReturnCountDayKey = "current_return_count_day";

        private const string LastVideoWatchTimeKey = "last_video_watch_time";
        private const string LastInterstitialWatchTimeKey = "last_interstitial_watch_time";

        private const string SessionsCountKey = "sessions_count";


        public float TotalMinutesInGame => default; // TODO


        public DateTime LastRealUtcTime => DateTime.UtcNow; // TODO


        public DateTime RealUtcTime => DateTime.UtcNow; // TODO

        public bool IsFirstLaunch => SessionsCount <= 1;

        public int SessionsCount
        {
            get => CustomPlayerPrefs.GetInt(SessionsCountKey, 0);

            private set
            {
                CustomPlayerPrefs.SetInt(SessionsCountKey, value);
            }
        }


        public int MatchesCount
        {
            get => CustomPlayerPrefs.GetInt(MatchesCountKey, 0);

            private set
            {
                CustomPlayerPrefs.SetInt(MatchesCountKey, value);
            }
        }


        public int TodayMatchesCount
        {
            get => CustomPlayerPrefs.GetInt(TodayMatchesCountKey, 0);
            private set => CustomPlayerPrefs.SetInt(TodayMatchesCountKey, value);
        }

        private DateTime TodayMatchesCountDate
        {
            get => CustomPlayerPrefs.GetDateTime(TodayMatchesCountDateKey);
            set => CustomPlayerPrefs.SetDateTime(TodayMatchesCountDateKey, value);
        }


        public int UniqueLevelsFinishedCount
        {
            get => CustomPlayerPrefs.GetInt(UniqueLevelsCountKey, 0);
            private set => CustomPlayerPrefs.SetInt(UniqueLevelsCountKey, value);
        }


        public DateTime FirstLaunchTime
        {
            get => CustomPlayerPrefs.GetDateTime(FirstLaunchTimeKey, DateTime.MinValue);
            private set => CustomPlayerPrefs.SetDateTime(FirstLaunchTimeKey, value);
        }
        


        public void Initialize()
        {
            if (!CustomPlayerPrefs.HasKey(FirstLaunchTimeKey))
            {
                FirstLaunchTime = DateTime.Now;
            }

            SessionsCount++;
        }


        public void IncrementMatchesCount()
        {
            MatchesCount++;

            bool needResetCounter = TimeUtility.IsAtLeastOneDayPassed(TodayMatchesCountDate, DateTime.Now);
            if (needResetCounter)
            {
                TodayMatchesCount = 0;
            }

            TodayMatchesCount++;
            TodayMatchesCountDate = DateTime.Now;
        }


        public void IncrementUniqueCount() =>
            UniqueLevelsFinishedCount++;
    }
}
